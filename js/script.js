function resize(){
	var window_hieght = $(window).height()
	$('#home').height(window_hieght);


	function equalwidgets1(){
      var h = 0;
     	$('#firstday ul li').outerHeight('auto');
        $('#firstday ul li').each(function(){
            var height = $(this).outerHeight();
            if(height > h){
                h = height;
            }
        });
        $('#firstday ul li').outerHeight(h);
    }
    equalwidgets1();

    function equalwidgets2(){
      var h = 0;
     	$('#secondday div.box').outerHeight('auto');
        $('#secondday div.box').each(function(){
            var height = $(this).outerHeight();
            if(height > h){
                h = height;
            }
        });
        $('#secondday div.box').outerHeight(h);
    }
    equalwidgets2();
}

$(document).ready(function(){
	
	$('#home header span.menu').click(function(){
		$('#home header nav.menu').slideDown(function(){
			$('#home header span.close').show()
		})
	});

	$('#home header span.close').click(function(){
		$('#home header nav.menu').slideUp(function(){
			$('#home header span.close').hide()
		})
	});

	$('nav.menu ul li a.location').click(function(){
		$('nav.menu').slideUp(function(){
			$('#home header span.close').hide()
		})
	})

	var wow = new WOW(
      {
        boxClass:     'wow',      // animated element css class (default is wow)
        animateClass: 'animated', // animation css class (default is animated)
        offset:       0,          // distance to the element when triggering the animation (default is 0)
        mobile:       true,       // trigger animations on mobile devices (default is true)
        live:         true,       // act on asynchronously loaded content (default is true)
        callback:     function(box) {
          // the callback is fired every time an animation is started
          // the argument that is passed in is the DOM node being animated
        },
        scrollContainer: null // optional scroll container selector, otherwise use window
      }
    );
    wow.init();

    $('a[href*="#"]:not([href="#"])').click(function() {
        if (location.pathname.replace(/^\//,'') == this.pathname.replace(/^\//,'') && location.hostname == this.hostname) {
            var target = $(this.hash);
            target = target.length ? target : $('[name=' + this.hash.slice(1) +']');
            if (target.length) {
                $('html, body').animate({
                  scrollTop: target.offset().top
                }, 1750);
                return false;
            }
        }
    });


	resize();
});

$(window).resize(function(){
	resize();
});

$(window).on('load',function(){
	resize();
});